package com.example.photoninterview

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import org.junit.Rule
import org.junit.Test
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.photoninterview.model.HighSchool
import com.example.photoninterview.ui.composables.MainScreen
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun testLoadingState() {
        composeTestRule.setContent {
            MainScreen(State.Loading)
        }

        composeTestRule
            .onNodeWithContentDescription("Loading")
            .assertIsDisplayed()
    }

    @Test
    fun testSuccessState() {
        composeTestRule.setContent {
            MainScreen(State.Successful(listOf(HighSchool(id = "1234", name = "High school of rock", description = "A description"))))
        }

        composeTestRule
            .onNodeWithText("1234")
            .assertIsDisplayed()
    }
}


