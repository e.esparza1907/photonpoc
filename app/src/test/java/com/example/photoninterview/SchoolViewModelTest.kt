package com.example.photoninterview

import org.junit.Test
import org.junit.Assert.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import app.cash.turbine.test
import com.example.photoninterview.model.HighSchool
import com.example.photoninterview.repository.HighSchoolRepository
import com.example.photoninterview.viewmodels.SchoolViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

@ExperimentalCoroutinesApi
class SchoolViewModelTest {

    private val highSchoolRepository = mock<HighSchoolRepository>()
    private lateinit var viewModel: SchoolViewModel

    private val testDispatcher = StandardTestDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `test getHighSchools success`() = runTest {
        // Arrange
        val schools = listOf(HighSchool(id = "test", name = "name", description = "descritpion"))
        `when`(highSchoolRepository.retrieveHighSchools()).thenReturn(Result.success(schools))

        viewModel = SchoolViewModel(highSchoolRepository)

        viewModel.getHighsSchools()

        viewModel.state.test {
            assertEquals(State.Loading, awaitItem())
            assertEquals(State.Successful(schools), awaitItem())
            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun `test getHighSchools failure`() = runTest {
        val errorMessage = "Network Error"
        `when`(highSchoolRepository.retrieveHighSchools()).thenReturn(
            Result.failure(
                RuntimeException(errorMessage)
            )
        )

        viewModel = SchoolViewModel(highSchoolRepository)

        viewModel.getHighsSchools()

        viewModel.state.test {
            assertEquals(State.Loading, awaitItem())
            assertEquals(State.Error(errorMessage), awaitItem())
            cancelAndIgnoreRemainingEvents()
        }
    }
}