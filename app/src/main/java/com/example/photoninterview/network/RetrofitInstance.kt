package com.example.photoninterview.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInstance {
    companion object {
        var retrofit = Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        var service: HighSchoolService = retrofit.create(HighSchoolService::class.java)
    }

}