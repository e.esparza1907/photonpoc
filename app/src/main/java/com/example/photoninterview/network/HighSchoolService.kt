package com.example.photoninterview.network

import com.example.photoninterview.repository.HighSchoolDTO
import retrofit2.Response
import retrofit2.http.GET

interface HighSchoolService {
    @GET("resource/s3k6-pzi2.json")
    suspend fun listHighSchools(): Response<List<HighSchoolDTO>>
}