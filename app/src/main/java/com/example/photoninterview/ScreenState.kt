package com.example.photoninterview

import com.example.photoninterview.model.HighSchool

sealed interface State {
    data class Successful(val listOfSchools: List<HighSchool> = listOf()) : State
    data class Error(val errorMessage: String) : State
    data object Loading : State
}
