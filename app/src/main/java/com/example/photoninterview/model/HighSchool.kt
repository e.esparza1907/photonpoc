package com.example.photoninterview.model

data class HighSchool(
    val id: String,
    val name: String,
    val description: String,
)
