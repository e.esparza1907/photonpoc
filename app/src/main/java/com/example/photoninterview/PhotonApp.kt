package com.example.photoninterview

import android.app.Application
import com.example.photoninterview.modules.mainModule
import org.koin.core.context.GlobalContext.startKoin

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(listOf(mainModule))
        }
    }
}