package com.example.photoninterview.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.photoninterview.State
import com.example.photoninterview.repository.HighSchoolRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class SchoolViewModel(private val highSchoolRepository: HighSchoolRepository = HighSchoolRepository()) :
    ViewModel() {

    private val _state = MutableStateFlow<State>(State.Loading)
    val state = _state.asStateFlow()

    fun getHighsSchools() {
        viewModelScope.launch {
            highSchoolRepository.retrieveHighSchools().onSuccess { schools ->
                _state.update { State.Successful(schools) }
            }.onFailure { exception ->
                _state.update { State.Error(exception.message.toString()) }
            }
        }
    }

}