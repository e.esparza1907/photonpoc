package com.example.photoninterview.modules

import com.example.photoninterview.repository.HighSchoolRepository
import org.koin.dsl.module

val mainModule = module {
    single {
        HighSchoolRepository()
    }
}