package com.example.photoninterview.repository

import com.example.photoninterview.model.HighSchool
import com.google.gson.annotations.SerializedName

data class HighSchoolDTO(
    @SerializedName("dbn")
    val id: String,
    @SerializedName("school_name")
    val name: String,
    @SerializedName("overview_paragraph")
    val description: String,
)

fun HighSchoolDTO.toDomain(): HighSchool {
    return HighSchool(
        name = name,
        description = description,
        id = id
    )
}