package com.example.photoninterview.repository

import com.example.photoninterview.network.RetrofitInstance.Companion.service
import com.example.photoninterview.model.HighSchool

class HighSchoolRepository {
    suspend fun retrieveHighSchools(): Result<List<HighSchool>> {
        return try {
            val response = service.listHighSchools()
            val body = response.body()
            if (response.isSuccessful && body != null) {
                Result.success(body.map { it.toDomain() })
            } else {
                Result.failure(RuntimeException("Error: ${response.code()} ${response.message()}"))
            }
        } catch (e: Exception) {
            Result.failure(e)
        }
    }
}