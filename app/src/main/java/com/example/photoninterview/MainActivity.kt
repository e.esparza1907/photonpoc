package com.example.photoninterview

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.photoninterview.ui.composables.MainScreen
import com.example.photoninterview.ui.theme.PhotonInterviewTheme
import com.example.photoninterview.viewmodels.SchoolViewModel

class MainActivity : ComponentActivity() {

    private val schoolViewModel: SchoolViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        schoolViewModel.getHighsSchools()

        setContent {
            val screenState = schoolViewModel.state.collectAsState().value
            MainScreen(screenState)
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    PhotonInterviewTheme {
        Greeting("Android")
    }
}
