package com.example.photoninterview.ui.composables

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import com.example.photoninterview.R
import com.example.photoninterview.State
import com.example.photoninterview.model.HighSchool
import com.example.photoninterview.ui.theme.PhotonInterviewTheme

@Composable
fun MainScreen(screenState: State) {
    PhotonInterviewTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            Column {
                Row(
                    Modifier
                        .height(56.dp)
                        .padding(horizontal = 8.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = stringResource(R.string.photon_example_app), fontSize = TextUnit(
                            22.0f,
                            TextUnitType.Sp
                        )
                    )
                }
                Divider(modifier = Modifier.fillMaxWidth(), thickness = 2.dp)

                when (screenState) {
                    is State.Successful -> {
                        LazyColumn {
                            items(items = screenState.listOfSchools) {
                                SchoolItem(it)
                            }
                        }
                    }

                    is State.Error -> {
                        ErrorScreen(screenState.errorMessage)
                    }

                    is State.Loading -> {
                        LoadingScreen()
                    }
                }
            }
        }
    }
}

@Composable
fun SchoolItem(school: HighSchool) {
    var isExpanded by remember { mutableStateOf(false) }


    Column(modifier = Modifier
        .fillMaxWidth()
        .clickable { isExpanded = !isExpanded }) {
        Text(text = school.name, fontWeight = FontWeight.Bold)
        Text(text = school.id)

        if (isExpanded) {
            Text(text = school.description)
        }
    }
}

@Composable
fun LoadingScreen() {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxSize()
            .semantics {
                contentDescription = "Loading"
            }
    ) {
        CircularProgressIndicator(
            modifier = Modifier.size(50.dp),
            color = Color.Black
        )
    }
}

@Composable
fun ErrorScreen(error: String) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Text(error)
    }
}